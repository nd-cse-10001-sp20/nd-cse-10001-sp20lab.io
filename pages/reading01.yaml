title:      "Reading 01: Information Representation"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, we will explore the mathematical underpinnings of digital
    computing by examining [boolean logic] and [binary numbers].  Likewise, we
    will further explore [Python] by learning about expressions, data types,
    and variables.

    <div class="alert alert-info" markdown="1">
    #### <i class="fa fa-bookmark"></i> TL;DR

    For this week, you need to read about how data is represented in a computer
    and about the basics of programming in [Python].  Afterwards, you will need
    the [quiz] below.
    </div>

    <img src="static/img/automate.png" class="img-responsive pull-right" style="height: 175px">
    <img src="static/img/digital.png" class="img-responsive pull-right" style="height: 175px">

    ## Readings

    The readings for this week are:

    1. [Understanding the Digital World]:

        - <p>2. Bits, Bytes, and Representation of Information</p>

    2. [Automate the Boring Stuff With Python]:

        - [Chapter 1 - Python Basics](https://automatetheboringstuff.com/chapter1/)

    <div class="alert alert-success" markdown="1">
    #### <i class="fa fa-terminal"></i> The Hands-on Imperative

    To get the most out of your reading, you should be **typing** commands into
    a [Python] interpreter and **playing** around with the things you reading.

    Passively reading will not be as fruitful as **actively** reading and
    trying out things you are exploring.
    </div>

    ## Quiz

    Once you have completed the readings, fill out the following [quiz]:

    <div class="text-center">
    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdXhj0i6yeoXKyhhx4KWvRC1d36fbEItRa1LSKDG03wTjlc-A/viewform?embedded=true" width="640" height="1875" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
    </div>

    If you cannot see the embedded [quiz] above, you can click on the button
    below to go to the [quiz] form:

    <div class="text-center">
      <p><a class="btn btn-primary" href="https://forms.gle/pkdGJien6L3Lk3WT9">Reading Quiz</a></p>
    </div>

    <div class="alert alert-danger" markdown="1">
    #### <i class="fa fa-warning"></i> Notre Dame Login

    To view and submit the form below, you need to be logged into your Notre Dame
    Google account.  The easiest way to do this is to login to
    [gmail.nd.edu](https://gmail.nd.edu) and then visit this page in the same
    browser session.

    </div>

    **Note**, you can view the initial quiz score after you submit your
    responses.  If you get any answers wrong, you can go back and adjust your
    answers as necessary.  After the deadline has passed, any wrong answers
    will be given partial credit.

    [Python]:                                   https://www.python.org/
    [Understanding the Digital World]:          http://kernighan.com/udw.html
    [Automate the Boring Stuff with Python]:    https://automatetheboringstuff.com/
    [binary codes]:                             https://en.wikipedia.org/wiki/Binary_code
    [data structures]:                          https://en.wikipedia.org/wiki/Data_structure
    [algorithms]:                               https://en.wikipedia.org/wiki/Algorithm
    [twitter]:                                  https://twitter.com/
    [quiz]:                                     https://forms.gle/pkdGJien6L3Lk3WT9
    [boolean logic]:                            https://en.wikipedia.org/wiki/Boolean_algebra
    [binary numbers]:                           https://en.wikipedia.org/wiki/Binary_number
    [logic gates]:                              https://en.wikipedia.org/wiki/Logic_gate
