title:      "Reading 02: CPU and Flow Control"
icon:       fa-book
navigation: []
internal:
external:
body:       |

    **Everyone**:

    Next week, we will explore how information is actually processed by the
    [CPU].  Likewise, we will also begin discussion flow control in [Python]
    by utilizing conditional execution.
    
    <div class="alert alert-info" markdown="1">
    #### <i class="fa fa-bookmark"></i> TL;DR

    For this week, you need to read about how data is processed by the [CPU]
    and about conditional execution in [Python].  Afterwards, you will need the
    [quiz] below.
    
    </div>

    <img src="static/img/automate.png" class="img-responsive pull-right" style="height: 175px">
    <img src="static/img/digital.png" class="img-responsive pull-right" style="height: 175px">

    ## Readings

    The readings for this week are:

    1. [Understanding the Digital World]:

        - <p>3. Inside the CPU</p>

            **Note**, you can access the [Toy CPU Simulator] here: [http://kernighan.com/toysim.html](http://kernighan.com/toysim.html)

        - <p>Wrapup on Hardware</p>

    2. [Automate the Boring Stuff With Python]:

        - [Chapter 2 - Flow Control](https://automatetheboringstuff.com/chapter2/)

            **Note**, you can skip the sections about **loops**.  We will cover
            those in the next assignment.  Just focus on conditional statements
            (i.e. `if`, `elif`, `else`).

        - [Chapter 3 - Functions](https://automatetheboringstuff.com/chapter3/)

    [Toy CPU Simulator]: http://kernighan.com/toysim.html

    <div class="alert alert-success" markdown="1">
    #### <i class="fa fa-terminal"></i> The Hands-on Imperative

    To get the most out of your reading, you should be **typing** commands into
    a [Python] interpreter and **playing** around with the things you reading.

    Passively reading will not be as fruitful as **actively** reading and
    trying out things you are exploring.
    </div>

    ## Quiz

    Once you have completed the readings, fill out the following [quiz]:

    <div class="text-center">
        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScAp9DAifDBLzP4GbMAqTHIGyXvCFy2W8naTYdD4jD1AX76aw/viewform?embedded=true" width="640" height="2205" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
    </div>

    If you cannot see the embedded [quiz] above, you can click on the button
    below to go to the [quiz] form:

    <div class="text-center">
        <p><a class="btn btn-primary" href="https://forms.gle/cg9Wz36CLWR4K6Zf7">Reading Quiz</a></p>
    </div>

    <div class="alert alert-danger" markdown="1">
    #### <i class="fa fa-warning"></i> Notre Dame Login

    To view and submit the form below, you need to be logged into your Notre Dame
    Google account.  The easiest way to do this is to login to
    [gmail.nd.edu](https://gmail.nd.edu) and then visit this page in the same
    browser session.

    </div>

    **Note**, you can view the initial quiz score after you submit your
    responses.  If you get any answers wrong, you can go back and adjust your
    answers as necessary.  After the deadline has passed, any wrong answers
    will be given partial credit.

    [Python]:                                   https://www.python.org/
    [Understanding the Digital World]:          http://kernighan.com/udw.html
    [Automate the Boring Stuff with Python]:    https://automatetheboringstuff.com/
    [binary codes]:                             https://en.wikipedia.org/wiki/Binary_code
    [data structures]:                          https://en.wikipedia.org/wiki/Data_structure
    [algorithms]:                               https://en.wikipedia.org/wiki/Algorithm
    [twitter]:                                  https://twitter.com/
    [quiz]:                                     https://forms.gle/cg9Wz36CLWR4K6Zf7
    [boolean logic]:                            https://en.wikipedia.org/wiki/Boolean_algebra
    [binary numbers]:                           https://en.wikipedia.org/wiki/Binary_number
    [CPU]:                                      https://en.wikipedia.org/wiki/Central_processing_unit
