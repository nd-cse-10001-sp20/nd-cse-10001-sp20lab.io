#!/usr/bin/env python3

import csv
import random
import glob
import sys
import yaml

TAS      = [ta['netid'] for ta in yaml.safe_load(open('static/yaml/ta.yaml'))]
STUDENTS = []

for student in csv.DictReader(open('static/csv/students.csv', 'r')):
    STUDENTS.append(student['Netid'])

CONFLICTS = dict(
    (ta['netid'], ta['conflicts'])
    for ta in yaml.safe_load(open('static/yaml/ta.yaml')) 
    if ta.get('conflicts')
)

random.shuffle(STUDENTS)
random.shuffle(TAS)

TAS     = TAS * (len(STUDENTS) // len(TAS) + 1)
MAPPING = list(sorted(map(list, zip(STUDENTS, TAS))))

for ta, conflicts in CONFLICTS.items():
    for conflict in conflicts:
        if [conflict, ta] in MAPPING:
            print('CONFLICT!')
            sys.exit(1)

print(yaml.dump(MAPPING, default_flow_style=False))
